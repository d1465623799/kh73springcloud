package com.kgc.cn.utils.activeMQ;

import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;
import javax.jms.Topic;


/**
 * Created by Ding on 2019/12/5.
 */
@Slf4j
@Component
public class ActiveMQUtils {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    /**
     * 点对点模式
     *
     * @param name
     * @param msg
     */
    public void sendMsgByQueue(String name, Object msg) {
        Queue queue = new ActiveMQQueue(name);
        jmsMessagingTemplate.convertAndSend(queue, msg);
    }

    /**
     * 发布订阅模式
     *
     * @param name
     * @param msg
     */
    public void sendMsgByTopic(String name, Object msg) {
        Topic topic = new ActiveMQTopic(name);
        jmsMessagingTemplate.convertAndSend(topic, msg);
    }
}
